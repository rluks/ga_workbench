package at.fhooe.im440.workbench.systems;

import com.badlogic.gdx.math.Vector2;

public interface IForce {

	public void ApplyForce(Vector2 force);
}
