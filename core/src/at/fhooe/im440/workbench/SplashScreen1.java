/*
 * Copyright (c) 2013 Roman Divotkey, Univ. of Applied Sciences Upper Austria. 
 * All rights reserved.
 * 
 * THIS CODE IS PROVIDED AS EDUCATIONAL MATERIAL AND NOT INTENDED TO ADDRESS
 * ALL REAL WORLD PROBLEMS AND ISSUES IN DETAIL.
 */

package at.fhooe.im440.workbench;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class SplashScreen1 extends ScreenAdapter {

	private static final Color BG_COL = new Color(1f, 0.38f, 0.22f, 1f);
	private static final float DISPLAY_TIME = 50.25f;

	private SpriteBatch batch;
	private Workbench workbench;
	private Texture texture;
	private OrthographicCamera camera;
	private Viewport viewport;
	
	private float displayTime;
	private boolean dead;
	
	public SplashScreen1(Workbench workbench) {
		this.workbench = workbench;
		this.batch = workbench.getBatch();
		this.camera = new OrthographicCamera();
		this.viewport = new FitViewport(800, 480, camera);
	}

	@Override
	public void show() {
		displayTime = DISPLAY_TIME;
		texture = new Texture("splash.png");
		dead = false;
	}	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(BG_COL.r, BG_COL.g, BG_COL.b, BG_COL.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		float w = texture.getWidth();
		float h = texture.getHeight();
		batch.draw(texture,  -w / 2,  -h / 2);
		batch.end();
		
		if (displayTime > 0) {
			displayTime -= delta;			
		} else {
			endOfState();
		}
	}

	private void endOfState() {
		if (dead) return;
		dead = true;
		workbench.setScreen(new MenuScreen(workbench));
	}

	@Override
	public void resize(int width, int height) {
		resizeCamera();
		viewport.update(width, height);
	}
	
	void resizeCamera(){
		System.out.println("resizeCamera");
		float ar = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
		camera.setToOrtho(false, Workbench.V_WIDTH, Workbench.V_WIDTH / ar);
		camera.position.set(0, 0, 0);
		camera.update();
		
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		texture.dispose();
	}

}
